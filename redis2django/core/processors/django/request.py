import json
import base64
from django.test.client import RequestFactory


class Redis2DjangoRequest():
    def __init__(self, content_bytes):
        content = json.loads(content_bytes.decode('utf8'))
        self.data = {}
        if content.get('query_arguments') is not None:
            self.data = content.get('query_arguments')
        if content.get('body') is not None:
            self.data = base64.standard_b64decode(content.get('body')).decode('utf8')
        self.HEADERS = {'HTTP_' + i[0].upper().replace('-', '_'): i[1] for i in content.get('headers').copy() }
        request_factory = RequestFactory(SERVER_NAME="localhost", **self.HEADERS)
        request = getattr(request_factory, content.get('method').lower())
        self.path = content.get('path')
        if "?" not in self.path:
            self.path = self.path.rstrip('/') + '/'
        request = request(self.path,  content_type=self.HEADERS.get('HTTP_CONTENT_TYPE'), data=self.data)
        self.redis_2_django_content = content
        self.request = request
