import time
import json
import base64


class ResponseMiddleWare():
    def process_request(self, request):
        return request
    
    def process_response(self, request, response):
        try:
            body = base64.standard_b64encode(response.content).decode('utf8')
        except:
            body = response.streaming_content
            body_text = ''
            for i in body:
                try:
                    body_text += i.decode('utf8')
                except:
                    body_text += i.decode('ascii', 'ignore')

            body = body_text
        response.redis2django = {
            'extra': request.redis_2_django_content.get('extra'),
            'status_code': response.status_code,
            'headers': [list(v) for k, v in response._headers.items()],
            'body': body,
        }
        response.PATH = request.path
        response.redis2django = json.dumps(response.redis2django)
        response.KEY = request.redis_2_django_content.get('extra').get('response_key')
        return response


class LogMiddleWare():
    def process_request(self, request):
        request.TIME_START = time.clock()
        return request
    
    def process_response(self, request, response):
        print('-'*80)
        print("{}: {} - status_code: {} - time: {} ms.".format(response.KEY, response.PATH, response.status_code, round((time.clock() - request.TIME_START) * 1000, 2)))
        return response