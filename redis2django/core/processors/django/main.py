from django.core.handlers.base import BaseHandler
from redis2python.core.processors.base import BaseProcessor
from redis2django.core.processors.django.request import Redis2DjangoRequest
from redis2django.core.processors.django.middleware import ResponseMiddleWare


class DjangoProcessor(BaseProcessor):
    handler = BaseHandler()
    handler.load_middleware()
    middleware = [ResponseMiddleWare, ]

    @classmethod
    def execute_django(cls, content_bytes):
        request = Redis2DjangoRequest(content_bytes)
        for mid in DjangoProcessor.middleware:
            request = mid().process_request(request)
        response = cls.handler.get_response(
            request.request
        )
        for mid in DjangoProcessor.middleware:
            response = mid().process_response(request, response)
        return response

    @classmethod
    async def process(cls, content_bytes, queue_name, loop, executor, aioredis_client_lpush):
        future = loop.run_in_executor(
            executor,
            cls.execute_django,
            content_bytes
        )
        response = await future
        aioredis_client_lpush.lpush(response.KEY, response.redis2django)
        return True
