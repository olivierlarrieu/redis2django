LOGO = '''
               _ _     ___     _ _                         
              | (_)   |__ \   | (_)                        
  _ __ ___  __| |_ ___   ) |__| |_  __ _ _ __   __ _  ___  
 | '__/ _ \/ _` | / __| / // _` | |/ _` | '_ \ / _` |/ _ \ 
 | | |  __/ (_| | \__ \/ /| (_| | | (_| | | | | (_| | (_) |
 |_|  \___|\__,_|_|___/____\__,_| |\__,_|_| |_|\__, |\___/ 2
                               _/ |             __/ |      
                              |__/             |___/       
'''

INFORMATIONS = '''
--------------------------------------------------------------------------------
An alternative from https://github.com/thefab/thr/tree/master/thr/redis2http.
Execute directly django code in the worker, without calling another http-server.
Author: Larrieu Olivier
Date: 11/03/2018
version: 1.0.0 beta
--------------------------------------------------------------------------------
'''