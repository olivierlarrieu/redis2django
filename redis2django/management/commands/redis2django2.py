from distutils.util import strtobool
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from redis2python.core.app import Redis2Python
from redis2django.core.processors.django import DjangoProcessor
from redis2django.core.processors.django.middleware import LogMiddleWare
from redis2django.core.processors.django.ascii_logo import LOGO, INFORMATIONS

# settings sample
# import mulitprocessing
# cpu_number = multiprocessing.cpu_count()
# REDIS2DJANGO_QUEUES = {
#     'thr:tasks_diff': [
#         {
#             "type": "process",
#             "max_workers": cpu_number,
#             "rlevel":0
#         },
#     ],
#     'thr:tasks_gene': [
#         {
#             "type": "process",
#             "max_workers": cpu_number,
#             "rlevel": 0
#         }
#     ],
#     'thr:notify': [
#         {
#             "type": "process",
#             "max_workers": cpu_number,
#             "rlevel": 0
#         }
#     ],
#     'thr:scheduler': [
#         {
#             "type": "thread",
#             "max_workers": cpu_number,
#             "rlevel": 0
#         },
#         {
#             "type": "thread",
#             "max_workers": cpu_number,
#             "rlevel": 1
#         }
#     ],
#     'thr:mcore': [
#         {
#             "type": "thread",
#             "max_workers": cpu_number,
#             "rlevel": 0
#         },
#         {
#             "type": "thread",
#             "max_workers": cpu_number,
#             "rlevel": 1
#         }
#     ],
# }
# REDIS2DJANGO_PROCESS_NAME = 'mcore'
# REDIS2DJANGO_UVLOOP = True
# REDIS2DJANGO_DEBUG = False
# REDIS2DJANGO_REDIS_HOST = 'redis://localhost'
# REDIS2DJANGO_REDIS_PORT = 6380



class Command(BaseCommand):
    help = 'start redis2django worker'

    def add_arguments(self, parser):
        parser.add_argument(
            '--name',
            dest='name',
            help='the process name',
        )

        parser.add_argument(
            '--debug',
            dest='debug',
            help='the process name',
        )

        parser.add_argument(
            '--queues',
            dest='queues',
            help='queues descriptions',
        )

        parser.add_argument(
            '--use_uvloop',
            dest='use_uvloop',
            help='use uvloop',
        )

        parser.add_argument(
            '--redis_host',
            dest='redis_host',
            help='redis host',
        )

        parser.add_argument(
            '--redis_port',
            dest='redis_port',
            help='redis port',
        )

        parser.add_argument(
            '--executor_type',
            dest='executor_type',
            help='thread or process',
        )

        parser.add_argument(
            '--max_recursion_level',
            dest='max_recursion_level',
            help='max recursion level',
        )

    def display_logo(self, process_name):
        print(LOGO)
        print(INFORMATIONS)
        print("process name: {}".format(process_name))


    def create_config(self, options):
        try:
            pname = '{}-{}'.format('redis2django', settings.REDIS2DJANGO_PROCESS_NAME)
        except:
            pname = None

        try:
            queues = settings.REDIS2DJANGO_QUEUES
        except:
            queues = None

        try:
            debug = settings.REDIS2DJANGO_DEBUG
        except:
            debug = None

        try:
            use_uvloop = settings.REDIS2DJANGO_UVLOOP
        except:
            use_uvloop = None

        try:
            redis_host = settings.REDIS2DJANGO_REDIS_HOST
        except:
            redis_host = None

        try:
            redis_port = settings.REDIS2DJANGO_REDIS_PORT
        except:
            redis_port = None

        try:
            executor_type = settings.REDIS2DJANGO_WORKER_TYPE
        except:
            executor_type = None

        if options.get('name') is not None:
            pname = '{}-{}'.format('redis2django', options.get('name'))

        if options.get('queues') is not None:
            queues = options.get('queues')

        if options.get('debug') is not None:
            debug = strtobool(options.get('debug'))

        if options.get('use_uvloop') is not None:
            use_uvloop = strtobool(options.get('use_uvloop'))

        if options.get('redis_host') is not None:
            redis_host = options.get('redis_host')

        if options.get('redis_port') is not None:
            redis_port = options.get('redis_port')

        if options.get('executor_type') is not None:
            executor_type = options.get('executor_type')

        assert queues is not None, 'queues is missing'
        pname = pname or 'redis2django2'
        config = {
            "name": pname,
            "queues": queues,
            "use_uvloop": use_uvloop,
            "debug": debug,
            "redis_host": redis_host,
            "redis_port": redis_port
        }
        kwargs = {k: v for k, v in config.items() if v is not None}
        return kwargs

    def handle(self, *args, **options):
        # get the config from parameters or django settings
        config = self.create_config(options)
        # choose the redis2python processor
        processor = DjangoProcessor
        # manage the processor middleware
        processor.middleware.append(LogMiddleWare)
        # create the Redis2Django worker
        Redis2Django = Redis2Python(processor=processor, **config)
        # display management command infos
        self.display_logo(Redis2Django.NAME)
        # run the loop
        Redis2Django.run()
