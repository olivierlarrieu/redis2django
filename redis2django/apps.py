from django.apps import AppConfig


class RedisToDjangoConfig(AppConfig):
    name = 'redis2django'
