from distutils.core import setup

setup(name='redis2django',
      version='1.0.0',
      description='asyncio redis worker which execute python django code under management command',
      url='https://olivierlarrieu@bitbucket.org/olivierlarrieu/redis2django.git',
      author='Larrieu Olivier',
      author_email='larrieuolivierad@gmail.com',
      license='MIT',
      install_requires=['redis2python', ],
      packages=['redis2django'],
      package_data = {'redis2django' : ['core/*', 'core/processors/*', 'core/processors/django/*', 'management/*', 'management/commands/*'] },
)
